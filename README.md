# Bodylight: The Alexander Technique

Based on the Alexander Technique and other movement coaching exercises Prof. Sarah Barker has developed, this app guides people through daily practice of self-care that will improve their physical wellbeing and mental ease.

## Video
Click the thumbnail below to watch the Bodylight functionality video

[![Bodylight Functionality](https://i9.ytimg.com/vi/0rY9ZpyGNXw/default.jpg?v=5adfbd0f&sqp=CPS2gtcF&rs=AOn4CLCC-jSnxSTLWjE165KNQPdNnZODpA)](https://www.youtube.com/watch?v=0rY9ZpyGNXw "Bodylight Functionality")

## Summary
Bodylight includes:
 * A relaxing and informational audio meditation guide.
 
 ![audioexersize](https://user-images.githubusercontent.com/35537376/39159574-460d2090-4734-11e8-88bb-d13732923bcb.PNG)
 
 * Video instruction on the Alexander Technique.
 
 ![video2](https://user-images.githubusercontent.com/35537376/39159587-5730951e-4734-11e8-8eb8-b275548d17a3.PNG)
 
 * Schedule reminders with google calendar to keep yourself on task.
 
 ![calander event](https://user-images.githubusercontent.com/35537376/39159619-87043ae8-4734-11e8-828e-a81109580ac2.PNG)
 ![calander event 2](https://user-images.githubusercontent.com/35537376/39159624-90437c90-4734-11e8-8bb8-a0ce88a5951f.PNG)
 
 * Set daily reminders for frequent practice.
 
 ![notifications](https://user-images.githubusercontent.com/35537376/39159629-966b7f14-4734-11e8-8051-ca0afa88e3be.PNG)
    
## Team
* [Pierce Matthews](https://github.com/EmperorMoose)
* [Christopher Waldron](https://github.com/cjwaldron18)
* [Juwan Jones](https://github.com/juwanlj)
* [Kenneth Richardson](https://github.com/kennethr95)
* [Justin Cusmano](https://github.com/CusmanoBlue)

## Repo
https://github.com/SCCapstone/Bodylight

## Testing

Packages used: package com.example.pmatt.bodylight
  
Testing the app
  1. Make sure [Android Studio](https://developer.android.com/studio/index.html) is installed and up to date as well as the apk.
  2. Create an emulator using the Android Studio avd manager or connect an android device to the avd manager. For help with this step go        to https://developer.android.com/studio/run/managing-avds.html
  3. Import the application and all correlated files into android studio and make sure it imports as a gradle build. For help with this        step go to https://developer.android.com/studio/intro/migrate.html   
  
  Name: WelcomeActivityTest.java
  Location: bodylight/app/src/androidTest/java/com/example/pmatt/bodylight/WelcomeActivityTest.java
  1. Inside android studio click Sync Project button in the top toolbar.
  2. Right click the test file within the project directory, click Run "WelcomeActivityTest", choose the android device you want to run        the test through, then click ok. This will run the test.
  3. IMPORTANT If you want to run the test multiple times be aware that the app must be uninstalled after each test.
  
  Name: MainActivityTest.java
  Location: bodylight/app/src/androidTest/java/com/example/pmatt/bodylight/MainActivityTest.java
  1. Inside android studio allow the project to be synced through gradle.
  2. Open the emulator and Bodylight application and then skip the welcome activity if it appears.
  3. Once at the main screen click the profile button and sign into a google account on the screen that appears.
  4. In android studio right click the test file within the project directory, click Run "MainActivityTest.java", choose the emulator, then click ok.  This will run the test.
  
  Name: AudioExerciseTest.java
  Location: bodylight/app/src/androidTest/java/com/example/pmatt/bodylight/AudioExerciseTest.java
  1. Inside android studio allow the project to be synced through gradle.
  2. Open the emulator and Bodylight application and then skip the welcome activity if it appears.
  3. In android studio right click the test file within the project directory, click Run "AudioExerciseTest.java", choose the emulator, then click ok. this will run the test.
  
  Name: VideoExerciseTest.java
  Location: bodylight/app/src/androidTest/java/com/example/pmatt/bodylight/VideoExerciseTest.java
  1. Inside android studio allow the project to be synced through gradle.
  2. Open the emulator and Bodylight application and then skip the welcome activity if it appears.
  3. In android studio right click the test file within the project directory, click Run "VideoExerciseTest.java", choose the emulator, then click ok. this will run the test.
  
  
