package com.euterpe.barker.bodylight.Adapter;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.euterpe.barker.bodylight.Fragments.AudioPlayerFragment;
import com.euterpe.barker.bodylight.Fragments.VideoFragment;
import com.euterpe.barker.bodylight.Interfaces.ItemClickListener;
import com.euterpe.barker.bodylight.R;

/**
 * Created by emperormoose on 2/28/18.
 */

public class VideoPlayerAdapter extends RecyclerView.Adapter<ViewHolder> {

    //This contains the names of the exercises
    private String[] exercise_list;
    private Context mContext;
    public TextView lenthText;
    public ImageView imageView;
    //Initializes the adapter
    public VideoPlayerAdapter(String[] data_list, Context context) {
        exercise_list = data_list;
        mContext = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_card, parent, false);
        lenthText = view.findViewById(R.id.watched_Text);
        imageView = view.findViewById(R.id.thumbnail);
        return new ViewHolder(view);
    }

    public int selectThumbnail(int position)
    {
        switch(position)
        {
            case 0: return R.drawable.video1;
            case 1: return R.drawable.video2;
            case 2: return R.drawable.video3;
            case 3: return R.drawable.video4;
            case 4: return R.drawable.video5;
        }
        return R.drawable.video1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
            //Sets the name to the name of the exercise
            String [] name_array = mContext.getResources().getStringArray(R.array.audio_exercise_list);
            String [] length_array = mContext.getResources().getStringArray(R.array.video_lengths_list);
            viewHolder.textView.setText(name_array[position]);
            SharedPreferences pref = mContext.getSharedPreferences("VideoFlags", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            String flagPos = "" + position;
            Boolean flag = pref.getBoolean(flagPos, false);
            int thumbnail = selectThumbnail(position);
            imageView.setImageResource(thumbnail);
            viewHolder.textView.setText(exercise_list[position]);
            viewHolder.lenthText.setText(length_array[position]);
        //Sets the onclick listener to pull up the meda player fragment when clicked
        viewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                AppCompatActivity appCompatActivity = (AppCompatActivity) view.getContext();

                if(!isLongClick) {
                    AudioPlayerFragment audioPlayerFragment = new AudioPlayerFragment();
                    Fragment fragment =
                            VideoFragment.newInstance(viewHolder.textView.getText());
                    appCompatActivity.getFragmentManager().beginTransaction()
                            .replace(R.id.fragment_placeholder, fragment,"findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }
            }
        });


    }
    @Override
    public int getItemCount() {
        return exercise_list.length;
    }
}

