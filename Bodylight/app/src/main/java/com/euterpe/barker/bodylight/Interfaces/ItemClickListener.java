/**
 * Created by CJ on 11/22/17.
 *  * Interface for ItemClickListener
 */

package com.euterpe.barker.bodylight.Interfaces;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);

}
