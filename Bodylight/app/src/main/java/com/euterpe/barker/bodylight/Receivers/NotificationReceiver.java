/**
 * Created by CJ on 12/2/17.
 */


package com.euterpe.barker.bodylight.Receivers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
//import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.euterpe.barker.bodylight.Bodylight;
import com.euterpe.barker.bodylight.MainActivity;
import com.euterpe.barker.bodylight.R;
//import com.google.android.exoplayer2.util.PriorityTaskManager;

import java.util.Random;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;


public class NotificationReceiver extends BroadcastReceiver {
    NotificationManager notificationManager;
    Intent notificationIntent;
    PendingIntent pendingIntent;
    //Above defines variables for use below

    public static final String CHANNEL_ID = "BODYLIGHT CHANNEL ID";
    private int MID = 1000;
    // This defines the channel name for use in Version 8 and above


    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Upon receiving the Alarm from SettingsFragment,
        // the Notification Manager creates a Notification for it

        String [] name_array = context.getResources().getStringArray(R.array.daily_reminder_list);
        //Below is the Version Build code, which signifies the differences between Versions 8 and Up or Below
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Version 8 and Up
            Notification.Builder builder = new Notification.Builder(context, CHANNEL_ID);
            Intent notificationIntent = new Intent(context, MainActivity.class);
            // This Section of the Code defines the Notification and defines the Intent for the Notification to Open

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(context,
                    MID,
                    notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            //Below builds the actual notification that will generate at the top of the Screen.
            int random = new Random().nextInt(name_array.length);
            String message = name_array[random];
            Notification notification = builder.setStyle(new Notification.BigTextStyle().bigText(message))
                    .setTicker("Alarm Alert!!")
                    .setSmallIcon(R.mipmap.ic_notification_logo)
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setAutoCancel(true)
                    .setChannelId(CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS)
                    //we set PendingIntent inside the ContentIntent so that the notification opens the app
                    .build();

            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    "Bodylight",
                    IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
            //Defines the Notification Channel
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(MID, notification);
            MID++;
            Toast.makeText(context, "Exercise Time!", Toast.LENGTH_LONG).show();
            //This generates a toast message once the Notification appears, and posts a notification to the Phone
        }
        else{
            //Version 7 and below
            //Defining the intents for use: note the lack of flags and channels
            notificationIntent = new Intent(context, MainActivity.class);
            pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

            int random = new Random().nextInt(name_array.length);
            String message = name_array[random];
            //Below defines the notification for Version 7 and Below
            Notification notification = new Notification.Builder(context)
                    .setContentTitle(message)
                    .setTicker("Alarm Alert!!")
                    .setSmallIcon(R.mipmap.ic_notification_logo)
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS)
                    //we set PendingIntent inside the ContentIntent so that the notification opens the app
                    .build();

            //Below launches the notification on screen for Version 7 and below, since there are no
            //channels in previous versions.
            notificationManager.notify(MID, notification);
            MID++;
            Toast.makeText(context, "Exercise Time!", Toast.LENGTH_LONG).show();
        }


    }

    /*
    This is the NotificationReceiver Class.
    This class is used for the SettingsFragment to generate a Notification and Notification Channel
    and generate it for the Device for all versions, focusing on the split that begins in Version 8.
     */

    }
