package com.euterpe.barker.bodylight;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.MenuItem;

import com.euterpe.barker.bodylight.Fragments.CalendarFragment;
import com.euterpe.barker.bodylight.Fragments.ContactFragment;
import com.euterpe.barker.bodylight.Fragments.EventCreationFrag;
import com.euterpe.barker.bodylight.Fragments.ExerciseViewFragment;
import com.euterpe.barker.bodylight.Fragments.InformationFragment;
import com.euterpe.barker.bodylight.Fragments.ProfileFragment;
import com.euterpe.barker.bodylight.Fragments.SettingsFragment;
import com.euterpe.barker.bodylight.Fragments.VideoFragment;
import com.euterpe.barker.bodylight.Fragments.VideoViewFragment;
import com.euterpe.barker.bodylight.Interfaces.DrawerLocker;

//Code begins, wow
public class MainActivity extends AppCompatActivity implements
    NavigationView.OnNavigationItemSelectedListener,
    VideoFragment.OnFragmentInteractionListener, DrawerLocker

    {

    private DrawerLayout drawer;
    private NavigationView navigationView;

    Toolbar toolbar;

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                                 DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        drawer.setDrawerLockMode(lockMode);
    }

    public boolean drawerExists()
    {
        if(drawer != null)
            return true;
        else
            return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Sets up the drawer
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Home");
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.main_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        setDrawerEnabled(true);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Loads the main exercises page
        Fragment fragment = new ExerciseViewFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_placeholder, fragment).commit();
    }

    //When the back button is pressed
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@Nullable MenuItem item) {

        findViewById(R.id.fragment_placeholder).setVisibility(View.VISIBLE);

        Fragment fragment = new Fragment();
        // For back navigation;
        getFragmentManager().popBackStack();

        //Handles drawer navigation
        // ** Create a function that automatically updates toolbar title from fragment title.**
        switch (item.getItemId()) {
            case R.id.nav_main_exercises:
                fragment = new ExerciseViewFragment();
                toolbar.setTitle("Allez-Up! Practice Sessions");
                break;
            case R.id.nav_profile:
                fragment = new ProfileFragment();
                toolbar.setTitle("Profile");
                break;
            case R.id.nav_calendar:
                fragment = new EventCreationFrag();
                toolbar.setTitle("Calendar");
                break;
            case R.id.nav_settings:
                fragment = new SettingsFragment();
                toolbar.setTitle("Reminders");
                break;
            case R.id.nav_videos:
                fragment = new VideoViewFragment();
                toolbar.setTitle("Video Tutorials");
                break;
            case R.id.nav_about:
                fragment = new InformationFragment();
                toolbar.setTitle("About");
                break;
            case R.id.nav_contact:
                fragment = new ContactFragment();
                toolbar.setTitle("Contact");
                break;
            case R.id.nav_exit:
                finish();
                System.exit(0);
                break;

        }

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_placeholder, fragment).addToBackStack(null).commit();

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
