package com.euterpe.barker.bodylight.Interfaces;

/**
 * Created by emperormoose on 4/26/18.
 */

public interface DrawerLocker {
    public void setDrawerEnabled(boolean enabled);
}
