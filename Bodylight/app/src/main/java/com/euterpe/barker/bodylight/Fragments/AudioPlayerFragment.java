    /**
     * This class handles all of the media player functions
     */

    package com.euterpe.barker.bodylight.Fragments;
    import com.euterpe.barker.bodylight.R;

    import android.content.SharedPreferences;
    import android.content.pm.ActivityInfo;
    import android.graphics.Color;
    import android.media.AudioManager;
    import android.media.MediaPlayer;
    import android.os.Bundle;
    import android.os.Handler;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.app.Fragment;
    import android.net.Uri;
    import android.widget.Button;
    import android.widget.TextView;
    import android.widget.Toast;

    public class AudioPlayerFragment extends Fragment implements View.OnClickListener {

        //The amount of selected_time by which to scrub
        private int forwardTime = 5000;
        private int backwardTime = 5000;

        //The name of the exercise
        private CharSequence exerciseName;

        //Used for showing playtime
        private double startTime = 0;
        private double finalTime = 0;

        //The buttons, test views
        private Button pause;
        private Button play;
        private Button reset;
        private Button foreward;
        private Button backward;
        private boolean paused;
        private boolean buffered = false;

        private TextView subtitle;
        private TextView exerciseTitle;

        private MediaPlayer mediaPlayer;

        //The progress bar at the bottom
        private Handler myHandler = new Handler();
        private com.budiyev.android.circularprogressbar.CircularProgressBar progressBar;

        //Used for storing the address of the file
        private String audioAddress;
        Uri audioUri;

        public AudioPlayerFragment() {

        }

        //Fragment constructor
        public AudioPlayerFragment newInstance(CharSequence exerciseName) {
            Bundle args = new Bundle();
            args.putCharSequence("exerciseName",exerciseName);
            AudioPlayerFragment fragment = new AudioPlayerFragment();
            fragment.setArguments(args);
            return fragment;
        }

        //Selects the appropriate file from the server and parses for playing
        //It is handled in the manner for minimal server pings
        public void selectFile(String exerciseName)
        {
            SharedPreferences pref = getContext().getSharedPreferences("AudioFlags", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();

            switch(exerciseName)
            {
                case "Allez-Up! Intro": audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1563237877/BodyLight_Intro.wav";
                                    audioUri = Uri.parse(audioAddress);
                                    break;
                case "Allez-Up! Basics":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540954933/Tape_1_Alexander_Technique_Directions.mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("0", true);
                                    editor.commit();
                                    break;
                case "Changing your Habits":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540954977/Tape_2_Using_a_pause_with_the_Directions..mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("1", true);
                                    editor.commit();
                                    break;
                case "Awareness Inside and Out":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540955023/Tape_3_Expand_your_awareness_in_self_and_space.mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("2", true);
                                    editor.commit();
                                    break;
                case "Sitting on your Sit-Bones":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540955064/Tape_4_Widening_and_Lengthening.mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("3", true);
                                    editor.commit();
                                    break;
                case "Freeing your Arms":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540955096/Tape_5_Raising_Arms.mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("4", true);
                                    editor.commit();
                                    break;
                case "Lengthen your Spine":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540955153/Tape_6_Rolling_Forward_in_the_Chair.mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("5", true);
                                    editor.commit();
                                    break;
                case "Expanding your Breath":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540955219/Tape_7_Expanding_your_breath.mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("6", true);
                                    editor.commit();
                                    break;
                case "Energizing your Legs":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540955280/Tape_8_Energizing_your_Legs.mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("7", true);
                                    editor.commit();
                                    break;
                case "Free the Jaw":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540955330/Tape_9_Freeing_the_Jaw.mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("8", true);
                                    editor.commit();
                                    break;
                case "Put it All Together":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540955387/Tape_10_Put_it_all_together.mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("9", true);
                                    editor.commit();
                                    break;
                case "Deeper Practice":  audioAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540954678/Deep_Relaxation.mp3";
                                    audioUri = Uri.parse(audioAddress);
                                    editor.putBoolean("10", true);
                                    editor.commit();
                                    break;
                default: audioAddress = "";
                     audioUri = Uri.parse(audioAddress);
                     break;
            }
        }

        //Gets the exercise name
        private void readBundle(Bundle bundle) {
            if (bundle != null) {
                exerciseName = bundle.getCharSequence("exerciseName");
            }
        }

        //Constructor
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            mediaPlayer = new MediaPlayer();

            readBundle(getArguments());
            selectFile(exerciseName.toString());

        }
        //constructor for when the view is created
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            //Inflate fragment
            View view = inflater.inflate(R.layout.activity_media_player, container, false);

            //Build seekbar
            progressBar = view.findViewById(R.id.progress_bar);
            progressBar.setProgress(0f);

            //construct all buttons and views
            play = view.findViewById(R.id.audio_play_button);
            reset = view.findViewById(R.id.reset);
            backward = view.findViewById(R.id.audio_backward_button);
            foreward = view.findViewById(R.id.audio_forward_button);
            //pause = view.findViewById(R.id.audio_pause_button);
            exerciseTitle = view.findViewById(R.id.exercise_title);
            subtitle = view.findViewById(R.id.subtitle);
            readBundle(getArguments());
            exerciseTitle.setText(exerciseName);
            String sub = getSubtitle((String) exerciseName);
            subtitle.setText(sub);
            paused = false;
            play.setOnClickListener(this);
            reset.setOnClickListener(this);
            backward.setOnClickListener(this);
            foreward.setOnClickListener(this);
            return view;
        }

        public String getSubtitle(String exerciseName)
        {
            String [] subtitle_array = getResources().getStringArray(R.array.subtitle_list);
            switch(exerciseName){
                case "Allez-Up! Intro":
                    return subtitle_array[0];
                case "Allez-Up! Basics":
                    return subtitle_array[1];
                case "Changing your Habits":
                    return subtitle_array[2];
                case "Awareness Inside and Out":
                    return subtitle_array[3];
                case "Sitting on your Sit-Bones":
                    return subtitle_array[4];
                case "Freeing your Arms":
                    return subtitle_array[5];
                case "Lengthen your Spine":
                    return subtitle_array[6];
                case "Expanding your Breath":
                    return subtitle_array[7];
                case "Energizing your Legs":
                    return subtitle_array[8];
                case "Free the Jaw":
                    return subtitle_array[9];
                case "Put it All Together":
                    return subtitle_array[10];
                case "Deeper Practice":
                    return subtitle_array[11];
            }
            return "subtitle";
        }

        //Switch case for finding which button was clicked
        @Override
        public void onClick(View view) {
            switch(view.getId()) {
                case R.id.audio_play_button:
                    if (mediaPlayer.isPlaying()) {
                        pauseFile();
                        break;
                    } else {
                        if (buffered == false) {
                            Toast.makeText(getView().getContext(), "Audio is Loading", Toast.LENGTH_LONG).show();
                            bufferFile();
                            break;
                        } else
                            playFile();
                            break;
                    }
                case R.id.reset:
                    reset();
                    break;
                case R.id.audio_backward_button:
                    scrubBackward();
                    break;
                case R.id.audio_forward_button:
                    scrubForward();
                    break;
            }
        }

        private void scrubForward() {
            int temp = mediaPlayer.getCurrentPosition();

            if((temp+forwardTime)<=finalTime){
                startTime = temp + forwardTime;
                mediaPlayer.seekTo((int) startTime);
                Toast.makeText(getContext(),
                        "You have Jumped forward 5 seconds",Toast.LENGTH_SHORT).show();
            }
        }

        //Scrubs backwards
        private void scrubBackward() {
            int temp = mediaPlayer.getCurrentPosition();
            Toast.makeText(getContext(),"You have Jumped backward 5 seconds",Toast.LENGTH_SHORT).show();
            if((temp-backwardTime)>0){
                startTime = temp - backwardTime;
                mediaPlayer.seekTo((int) startTime);
                Toast.makeText(getContext(),"You have Jumped backward 5 seconds",Toast.LENGTH_SHORT).show();
            }
        }

        public void reset()
        {
            Toast.makeText(getView().getContext(), "Reset Audio", Toast.LENGTH_LONG).show();
            mediaPlayer.stop();
            mediaPlayer.seekTo(0);
            bufferFile();
        }
        //Stops the media
        @Override
        public void onStop () {
            super.onStop();
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                play.setText(">");
                play.setTextColor(Color.WHITE);
                paused = true;
            }
        }

        private  void bufferFile() {
            //mediaPlayer = MediaPlayer.create(getContext(), audioUri);
            mediaPlayer.reset();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try { mediaPlayer.setDataSource(getContext(), audioUri); }
            catch (Exception e) {}
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    playFile();
                }
            });
            mediaPlayer.prepareAsync();
            buffered = true;
            playFile();
        }
        //Plays file
        private void playFile() {

            mediaPlayer.start();

            paused = false;
            //Song metadata
            finalTime = mediaPlayer.getDuration();
            startTime = mediaPlayer.getCurrentPosition();

            //Progresses the seekbar with the current selected_time
            //progressBar.setProgress((float)startTime);
            myHandler.postDelayed(UpdateSongTime,100);
            play.setText("||");
            play.setTextColor(Color.WHITE);
        }

        //Pauses
        private void pauseFile() {
            mediaPlayer.pause();
            play.setText(">");
            play.setTextColor(Color.WHITE);
            paused = true;
        }

        //Keeps the seekbar updated
        private Runnable UpdateSongTime = new Runnable() {
            public void run() {
                startTime = mediaPlayer.getCurrentPosition();
                startTime = startTime / mediaPlayer.getDuration();
                progressBar.setProgress((float) startTime * 100);
                myHandler.postDelayed(this, 100);
            }
        };
    }
