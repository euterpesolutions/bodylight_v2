package com.euterpe.barker.bodylight.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Fragment;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

import com.euterpe.barker.bodylight.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


public class ProfileFragment extends Fragment implements View.OnClickListener {

    //Local Variables
    private static final String kTAG = "ProfileFragment";
    private static final int RC_SIGN_IN = 9999;
    private final String defaultName = "Account Name Not Found!";
    private final String defaultEmail= "Account Email Not Found!";
    private GoogleSignInClient mGoogleSignInClient;

    private TextView nameView;
    private TextView emailView;
    private ImageView imageView;
    SignInButton GBtn;
    private Button signOut;


    // Default constructor.
    public ProfileFragment () {

    }

    @Override
    public void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getContext());
        updateUI(account);
    }

    //Creation constructor
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!CheckGoogleServices()) {
            Log.d(kTAG, "Update Google Services");
            getFragmentManager().popBackStack();
        }

        GoogleSignInOptions googleSignInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), googleSignInOptions);

    }

    //Displays user info
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Profile");

        // Inflate layout;
        View view = inflater.inflate(R.layout.content_profile, container, false);

        GBtn = (SignInButton) view.findViewById(R.id.googleBtn);
        nameView = view.findViewById(R.id.profile_display_name);
        emailView = view.findViewById(R.id.profile_email);
        imageView = view.findViewById(R.id.profile_image);

        signOut =  view.findViewById(R.id.sign_out_button);

        view.findViewById(R.id.googleBtn).setOnClickListener(this);

        signOut.setOnClickListener(this);


        GoogleSignInAccount account_ = GoogleSignIn.getLastSignedInAccount(getContext());
        updateUI(account_);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.googleBtn:
                signIn();
                break;
            case R.id.sign_out_button:
                signOut();
                break;
        }
    }

    private void updateUI(GoogleSignInAccount account_) {

        if (account_ != null) {
            // Account is signed in.

            GBtn.setVisibility(View.GONE);
            signOut.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);

            nameView.setText(account_.getDisplayName());
            emailView.setText(account_.getEmail());
            Picasso.with(getContext()).load(account_.getPhotoUrl()).into(imageView);

        } else {
            // Account is not signed in.
            GBtn.setVisibility(View.VISIBLE);
            signOut.setVisibility(View.GONE);
            Picasso.with(getContext()).load(R.drawable.ic_nav_header).into(imageView);

            nameView.setText(defaultName);
            emailView.setText(defaultEmail);
        }
    }

    private void signIn() {

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete( Task<Void> task) {
                        // [START_EXCLUDE]
                        updateUI(null);
                        // [END_EXCLUDE]
                    }
                });
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in.
            updateUI(account);
            reloadFragment();
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(kTAG, "signInResult:failed code=" + e.getStatusCode());
            reloadFragment();
            updateUI(null);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void reloadFragment() {
        getFragmentManager().beginTransaction().detach(this).attach(this).commit();
    }

    private boolean CheckGoogleServices(){
        int isUpdated = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(getContext());
        if (isUpdated == ConnectionResult.SUCCESS) {
            Log.d(kTAG,"Google Services Up-To-Date");
            return true;
        } else {
            Log.e(kTAG, "Error: Invalid Google Services");
            Dialog dialog = new Dialog(getContext());
            dialog.setContentView(R.layout.alert_dialog);
            dialog.show();

            return false;
        }
    }
}
