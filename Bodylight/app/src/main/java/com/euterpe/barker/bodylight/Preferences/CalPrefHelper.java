package com.euterpe.barker.bodylight.Preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.TextView;

import com.euterpe.barker.bodylight.Objects.CalendarObject;

import java.util.Map;

/**
 * Created by CJ on 3/27/18.
 * This will hold calender preference access.
 */

public class CalPrefHelper {

    private static final String kTAG = "CalPrefHelper";
    private static final String CALENDAR_PREFERENCE_FILE
            = "com.bodylight.CALENDAR_EVENTS_PREFERENCES";
    private static final String TEMP_DATE = "TEMP_DATE";
    private static final String TEMP_TIME = "TEMP_TIME";
    private static final String TEMP_EXERCISE = "TEMP_EXERCISE";

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    public CalPrefHelper (Context context) {
        sharedPreferences = context.getSharedPreferences(CALENDAR_PREFERENCE_FILE,
                Context.MODE_PRIVATE);
    }

    public void AddEventPreference(CalendarObject event) {
        editor = sharedPreferences.edit();
        editor.putString(event.getDate_time_key(), event.getExercise_name());
        editor.apply();
    }

    public void ReadAllEventPreferences(TextView textView){
        textView.setText("");
        Map<String, ?> event_list_map = sharedPreferences.getAll();
        for (Map.Entry<String, ?> entry : event_list_map.entrySet()) {
            String temp_entry = entry.getKey() + " - " + entry.getValue() + "\n";
            Log.d(kTAG, entry.getKey() + " - " + entry.getValue());
            textView.append(temp_entry);
        }
    }

    public void findEvent (TextView textView, String date_to_find) {
        textView.setText("");
        Map<String, ?> event_list_map = sharedPreferences.getAll();
        for (Map.Entry<String, ?> entry : event_list_map.entrySet()) {
            if (date_to_find.equals(getKeyDate(entry.getKey()))) {
                String temp_entry = entry.getKey() + " - " + entry.getValue() + "\n";
                Log.d(kTAG, entry.getKey() + " - " + entry.getValue());
                textView.append(temp_entry);
            }
        }
    }

    public void removeAllPreferences () {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    private String getKeyDate (String string_to_parse) {
        String[] tokens = string_to_parse.split("[ ]");
        return tokens[0];
    }
}
