package com.euterpe.barker.bodylight.Preferences;

import android.preference.DialogPreference;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.NumberPicker;
import android.view.View;
import com.euterpe.barker.bodylight.R;

/**
 * Created by CJ on 11/27/17.
 */

public class FrequencyPickerPreference extends DialogPreference {

    public static final int MIN_VALUE = 0;
    public static final int MAX_VALUE = 2;
    public static final int DEFAULT_VALUE = 0;
    public static final String[] VALUES = {"Daily", "Hourly", "Random"};
    public static String pickedValue;

    public static NumberPicker numberPicker;
    private int pickedNumber;

    //Getter
    public int getPickedNumber() {
        return pickedNumber;
    }

    //Setter
    public void setPickedNumber(int pickedNumber) {
        this.pickedNumber = pickedNumber;
    }

    public FrequencyPickerPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setDialogLayoutResource(R.layout.number_picker);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

    }


    @Override
    public void onBindDialogView(View view){
        numberPicker = view.findViewById(R.id.number_picker);

        numberPicker.setMinValue(MIN_VALUE);
        numberPicker.setMaxValue(MAX_VALUE);
        numberPicker.setValue(getPersistedInt(DEFAULT_VALUE));
        numberPicker.setDisplayedValues(VALUES);

        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            setPickedNumber(numberPicker.getValue());
            pickedValue = VALUES[numberPicker.getValue()];
            persistInt(getPickedNumber());
        }
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object object) {
        setPickedNumber(restoreValue ? getPersistedInt(DEFAULT_VALUE) : (Integer) object);
    }
}
