    package com.euterpe.barker.bodylight.Fragments;

    import android.app.Fragment;
    import android.content.Context;
    import android.content.pm.ActivityInfo;
    import android.os.Bundle;
    import android.support.v7.app.AppCompatActivity;
    import android.support.v7.widget.LinearLayoutManager;
    import android.support.v7.widget.RecyclerView;
    import android.support.v7.widget.Toolbar;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;

    import com.euterpe.barker.bodylight.Adapter.VideoPlayerAdapter;
    import com.euterpe.barker.bodylight.Interfaces.DrawerLocker;
    import com.euterpe.barker.bodylight.R;


    /**
     * Created by emperormoose on 2/28/18.
     */


    public class VideoViewFragment extends Fragment {
        private RecyclerView recyclerView;
        private RecyclerView.Adapter adapter;
        private RecyclerView.LayoutManager layoutManager;

        private String[] exercise_list;
        private int exercise_count = 5;

        private Context context;
        public VideoViewFragment() {
            // Required empty public constructor
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            initializeData();

        }

        //Creation constructor
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            toolbar.setTitle("Video Tutorials");
            //Upon clicking and opening video fragment, Toolbar title is changed


            // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.fragment_exercise_view, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

            layoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(layoutManager);

            // Pass data to adapter/recycler view.
            adapter = new VideoPlayerAdapter(exercise_list, this.context);
            recyclerView.setAdapter(adapter);
            ((DrawerLocker) getActivity()).setDrawerEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().show();
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            return view;
        }

        //Used to create the list of exercise cards
        private void initializeData() {
            exercise_list = new String[exercise_count];
            context = this.getContext();
            String[] foo_array = context.getResources().getStringArray(R.array.video_exercise_list);
            for (int i = 0; i < exercise_count; i++) {
                exercise_list[i] = foo_array[i];
            }
        }
    }
