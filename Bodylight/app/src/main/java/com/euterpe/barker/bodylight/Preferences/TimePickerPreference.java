package com.euterpe.barker.bodylight.Preferences;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

import com.euterpe.barker.bodylight.R;

/**
 * Created by CJ on
 * 12/2/17.
 */

public class TimePickerPreference extends DialogPreference {

    private int hour;
    private int minute;
    private static String time;
    public static TimePicker timePicker;

    //Getters and setters
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public TimePickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDialogLayoutResource(R.layout.timepicker);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);

        timePicker = view.findViewById(R.id.time_picker);

        //timePicker.setHour(hour);
        //timePicker.setMinute(minute);

    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        Preference current_alarm = this.findPreferenceInHierarchy("current_alarm");

        if (positiveResult) {
            setHour(timePicker.getHour());
            setMinute(timePicker.getMinute());
            time = Integer.toString(getHour()) + ":" + Integer.toString(getMinute());
            persistString(time);
            time = format_time(getHour(), getMinute());
            current_alarm.setTitle(time);
            persistString(time);
        }
    }

    private String format_time(Integer hour, Integer minute)
    {
        String time;
        String strMinute = minute.toString();
        if(hour > 12)
            hour = hour - 12;
        if(minute < 10)
            strMinute = "0" + strMinute;
        time = "Current Alarm: " + hour.toString() + ":" + strMinute;
        return time;
    }

    @Override
    protected Object onGetDefaultValue(TypedArray typedArray, int index) {
        return(typedArray.getString(index));
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object object) {

    }

}
