    package com.euterpe.barker.bodylight.Fragments;

    import android.content.Context;
    import android.content.SharedPreferences;
    import android.content.pm.ActivityInfo;
    import android.content.res.Configuration;
    import android.media.MediaPlayer;
    import android.net.Uri;
    import android.os.Bundle;
    import android.app.Fragment;
    import android.support.v7.app.AppCompatActivity;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.MediaController;
    import android.widget.Toast;
    import android.widget.VideoView;

    import com.euterpe.barker.bodylight.Interfaces.DrawerLocker;
    import com.euterpe.barker.bodylight.MainActivity;
    import com.euterpe.barker.bodylight.R;
    import com.google.android.exoplayer2.ExoPlayer;
    import com.google.android.exoplayer2.ExoPlayerFactory;
    import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
    import com.google.android.exoplayer2.extractor.ExtractorsFactory;
    import com.google.android.exoplayer2.source.ExtractorMediaSource;
    import com.google.android.exoplayer2.source.MediaSource;
    import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
    import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
    import com.google.android.exoplayer2.trackselection.TrackSelection;
    import com.google.android.exoplayer2.trackselection.TrackSelector;
    import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
    import com.google.android.exoplayer2.upstream.BandwidthMeter;
    import com.google.android.exoplayer2.upstream.DataSource;
    import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
    import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
    import com.google.android.exoplayer2.util.Util;

    /**
     * A simple {@link Fragment} subclass.
     * Activities that contain this fragment must implement the
     * {@link VideoFragment.OnFragmentInteractionListener} interface
     * to handle interaction events.
     * Use the {@link VideoFragment#newInstance} factory method to
     * create an instance of this fragment.
     */
    public class VideoFragment extends Fragment {
        // TODO: Add Local Vars

        private OnFragmentInteractionListener mListener;
        private CharSequence videoName;
        private String videoAddress;
        private Uri videoUri;

        private Context mContext = this.getContext();

        public VideoFragment() {
            // Required empty public constructor
        }

        // TODO: Rename and change types and number of parameters
        public static VideoFragment newInstance(CharSequence videoName) {
            Bundle args = new Bundle();
            args.putString("videoName", (String) videoName);
            VideoFragment fragment = new VideoFragment();
            fragment.setArguments(args);
            return fragment;
        }

        //Selects the appropriate file from the server and parses for playing
        //It is handled in the manner for minimal server pings
        public void selectFile(String exerciseName)
        {
            SharedPreferences pref = getContext().getSharedPreferences("VideoFlags", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            switch(exerciseName)
            {
                case "Turning your Head":  videoAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1563238898/Head_and_Neck.mp4";
                    videoUri = Uri.parse(videoAddress);
                    editor.putBoolean("0", true);
                    editor.commit();
                    break;
                case "Freeing your Hips":  videoAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1563238145/Tips_for_Hips.mp4";
                    videoUri = Uri.parse(videoAddress);
                    editor.putBoolean("1", true);
                    editor.commit();
                    break;
                case "Lengthening your Arms":  videoAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540953945/Moving_Your_Arms.mp4";
                    editor.putBoolean("2", true);
                    videoUri = Uri.parse(videoAddress);
                    editor.commit();
                    break;
                case "Rolling your Spine":  videoAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1545514647/The_Spine_1_1.mp4";
                    editor.putBoolean("3", true);
                    videoUri = Uri.parse(videoAddress);
                    editor.commit();
                    break;
                case "Freeing your Jaw":  videoAddress = "https://res.cloudinary.com/euterpe-solutions/video/upload/v1540953906/Jaw.mp4";
                    editor.putBoolean("4", true);
                    videoUri = Uri.parse(videoAddress);
                    editor.commit();
                    break;
            }
        }

        //Gets the exercise name
        private void readBundle(Bundle bundle) {
            if (bundle != null) {
                videoName = bundle.getCharSequence("videoName");
            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            readBundle(getArguments());
            selectFile(videoName.toString());
            if((getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                    && (((MainActivity)getActivity()).drawerExists() == true))
            {
                ((DrawerLocker) getActivity()).setDrawerEnabled(false);
            }

            Toast.makeText(getActivity(), "Video is Loading", Toast.LENGTH_LONG).show();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
           View view = inflater.inflate(R.layout.fragment_video, container, false);



           VideoView videoView = (VideoView)view.findViewById(R.id.videoView);
           MediaController mediaController = new MediaController(this.getContext());
           mediaController.setAnchorView(videoView);
           videoView.setMediaController(mediaController);
           videoView.setVideoURI(videoUri);

            videoView.setOnPreparedListener(
                    new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {
                            videoView.start();
                        }
                    });

           videoView.requestFocus();
           getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
           ((AppCompatActivity)getActivity()).getSupportActionBar().hide();

           videoView.start();

           return view;
        }

        // TODO: Rename method, update argument and hook method into UI event
        public void onButtonPressed(Uri uri) {
            if (mListener != null) {
                mListener.onFragmentInteraction(uri);
            }
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if (context instanceof OnFragmentInteractionListener) {
                mListener = (OnFragmentInteractionListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement OnFragmentInteractionListener");
            }
        }

        @Override
        public void onDetach() {
            super.onDetach();
            mListener = null;
        }

        /**
         * This interface must be implemented by activities that contain this
         * fragment to allow an interaction in this fragment to be communicated
         * to the activity and potentially other fragments contained in that
         * activity.
         * <p>
         * See the Android Training lesson <a href=
         * "http://developer.android.com/training/basics/fragments/communicating.html"
         * >Communicating with Other Fragments</a> for more information.
         */
        public interface OnFragmentInteractionListener {
            public void onFragmentInteraction(Uri uri);
        }
    }
