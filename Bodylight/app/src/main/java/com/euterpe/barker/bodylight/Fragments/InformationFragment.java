package com.euterpe.barker.bodylight.Fragments;

/**
 * Created by CJ on 12/6/17.
 */
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.euterpe.barker.bodylight.R;

public class InformationFragment extends Fragment {

    private View view;
    TextView link;
    Spanned Text;

    public InformationFragment() {

    }

    //Constructor
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("What is Allez-Up!");
        // Upon entering the Information Fragment, Toolbar title is changed to Information
        view = inflater.inflate(R.layout.content_information, container, false);
        link = (TextView)view.findViewById(R.id.linkText);
        link.setMovementMethod(LinkMovementMethod.getInstance());
        return view;
    }

}
