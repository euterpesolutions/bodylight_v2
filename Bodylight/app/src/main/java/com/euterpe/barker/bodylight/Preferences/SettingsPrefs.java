package com.euterpe.barker.bodylight.Preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by CJ on 12/4/17.
 */

//Setting getters and setters
public class SettingsPrefs {

    private final String KEY_PREF_FILE = "myPrefs";
    private final String KEY_FREQUENCY = "notification_frequency";
    private final String KEY_TIME = "notification_time";
    private final String KEY_NOTIF_BOOL = "isEnabled";


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public SettingsPrefs(Context context) {
        sharedPreferences = context.getSharedPreferences(KEY_PREF_FILE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void apply() {
        editor.apply();
    }

    public String getFrequency() {
        return sharedPreferences.getString(KEY_FREQUENCY, "Daily");
    }

    public void setFrequency(String frequency) {
        editor.putString(KEY_FREQUENCY, frequency);
    }

    public void setNotificationStatus(Boolean bool) {editor.putBoolean(KEY_NOTIF_BOOL, bool);}

    public String getTime() {
        return sharedPreferences.getString(KEY_TIME, "HH:MM AM/PM");
    }

    public void setTime(String time) {
        editor.putString(KEY_TIME, time);
    }


}
