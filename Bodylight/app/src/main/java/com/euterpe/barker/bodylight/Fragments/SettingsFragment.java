/**
 * Created by CJ on 11/24/17.
 *
 */

package com.euterpe.barker.bodylight.Fragments;


import android.app.AlarmManager;
import android.content.Context;
import android.os.Bundle;
import android.app.PendingIntent;
import android.preference.PreferenceFragment;
import android.preference.Preference;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.SharedPreferences;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import com.euterpe.barker.bodylight.Preferences.FrequencyPickerPreference;
import com.euterpe.barker.bodylight.Preferences.SettingsPrefs;
import com.euterpe.barker.bodylight.Preferences.TimePickerPreference;
import com.euterpe.barker.bodylight.R;
import com.euterpe.barker.bodylight.Receivers.NotificationReceiver;


import java.util.Calendar;
import java.util.Objects;

/*
This class handles all functionality of the settings fragment. Namely notifications.
 */
public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {

    //Setting strings
    private final String KEY_PREF_FILE = "myPrefs";
    private final String KEY_NOTIFICATION = "enable_notifications";
    private final String KEY_FREQUENCY = "notification_frequency";
    private final String KEY_TIME = "notification_time";

    //Local Variables
    private String frequency;
    private int hour;
    private int minute;
    private String time;
    private Calendar calendar;
    private Preference preference;
    private SettingsPrefs settingsPrefs;
    private AlarmManager alarmManager;
    private Intent notificationIntent;
    private PendingIntent pendingIntent;

    //Creation Constructor
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Settings");
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Reminders");
        //Upon entering the Settings Fragment, Toolbar title is changed to Settings

        addPreferencesFromResource(R.xml.pref_settings);
        //Get settings from the xml

        settingsPrefs = new SettingsPrefs(getContext());

        //Get settings and set
        frequency = settingsPrefs.getFrequency();
        time = settingsPrefs.getTime();

        preference = findPreference(KEY_FREQUENCY);
        preference.setSummary(frequency);

        preference = findPreference(KEY_TIME);
        preference.setSummary(time);

        calendar = Calendar.getInstance();

        //Initially sets the current alarm
        if (alarm_exists()) {
            setAlarmText(settingsPrefs.getTime());
        } else {
            setAlarmText("No Alarm Set");
        }

        //Cancel alarm button
        Preference alarm_button = findPreference("cancel_alarms");
        alarm_button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (alarm_exists()) {
                    cancelAlarms();
                    return true;
                } else
                    return false;
            }
        });
    }

    //This formats the time for setting the text
    private String format_time(Integer hour, Integer minute)
    {
        String time;
        String strMinute = minute.toString();
        if(hour > 12)
            hour = hour - 12;
        if(minute < 10)
            strMinute = "0" + strMinute;
        time = "Current Alarm: " + hour.toString() + ":" + strMinute;
        return time;
    }
    //Returns true if a notification exists
    private boolean alarm_exists()
    {
        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
        notificationIntent = new Intent(getContext(), NotificationReceiver.class);
        boolean alarmUp = (PendingIntent.getBroadcast(getContext(), 1001, notificationIntent,
                PendingIntent.FLAG_NO_CREATE) != null);
        return alarmUp;
    }

    //Cancels the alarms and updates the current alarm field
    private void cancelAlarms()
    {
        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent(getContext(), NotificationReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(getContext(), 1001, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
        time = null;
        preference.setSummary(time);
        settingsPrefs.setTime(time);
        settingsPrefs.setNotificationStatus(true);
        settingsPrefs.apply();
        Toast.makeText(getContext(), "Alarm Cancelled!", Toast.LENGTH_LONG).show();
        setAlarmText("No Alarm Set");
    }

    //Sets the current alarm text
    private void setAlarmText(CharSequence text)
    {
        Preference current_alarm = findPreference("current_alarm");
        current_alarm.setTitle(text);
    }

    //When prefs are changed
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        preference = findPreference(key);
        settingsPrefs = new SettingsPrefs(getContext());

        //Find which has been changed
        switch (key) {
            case KEY_FREQUENCY:
                //Set preferences
                frequency = FrequencyPickerPreference.pickedValue;
                preference.setSummary(frequency);
                settingsPrefs.setFrequency(frequency);
                break;
            case KEY_TIME:
                //Set preferences
                hour = TimePickerPreference.timePicker.getHour();
                minute = TimePickerPreference.timePicker.getMinute();
                time = format_time(hour, minute);
                setAlarmText(time);
                preference.setSummary(time);
                settingsPrefs.setTime(time);
                settingsPrefs.setNotificationStatus(true);
                settingsPrefs.apply();

                //Start the Alarm Manager
                alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
                //Send the Notification to the Receiver
                notificationIntent = new Intent(getContext(), NotificationReceiver.class);

                pendingIntent = PendingIntent.getBroadcast(getContext(), 1001, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                //This block sets the current time for alarm text
                setAlarmText(settingsPrefs.getTime());
                //Sets the time for the alarm
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);

                //Sets the Pending intent, which defines the channel (1001) and the Updates the Flag
                if (Objects.equals(frequency, "Daily")) {
                    //Daily Alarm
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                            AlarmManager.INTERVAL_DAY, pendingIntent);
                    Toast.makeText(getContext(), "Daily Alarm Set!", Toast.LENGTH_LONG).show();
                    break;
                } else if (Objects.equals(frequency, "Hourly")) {
                    //Weekly Alarm
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                            AlarmManager.INTERVAL_HOUR, pendingIntent);
                    Toast.makeText(getContext(), "Hourly Alarm Set!", Toast.LENGTH_LONG).show();
                    break;
                } else if (Objects.equals(frequency, "Random")) {
                    //Random Alarm
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                            AlarmManager.INTERVAL_HALF_DAY, pendingIntent);
                    Toast.makeText(getContext(), "Random Alarm Set!", Toast.LENGTH_LONG).show();
                    break;
                }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }



}
