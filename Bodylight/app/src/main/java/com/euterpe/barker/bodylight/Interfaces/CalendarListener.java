package com.euterpe.barker.bodylight.Interfaces;

import java.util.Date;

/**
 * Created by CJ on 2/11/18.
 */

public interface CalendarListener {
    void onDateSelect(Date date);
    void onMonthChange(Date time);
}
