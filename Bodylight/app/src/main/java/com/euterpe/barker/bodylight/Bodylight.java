package com.euterpe.barker.bodylight;

import android.app.Application;
import android.provider.Settings;

/**
 * Created by emperormoose on 4/6/18.
 */

//This class was created to contain global variables. Chiefly the flag for whether or not a video has been watched.
public class Bodylight extends Application {

    private Boolean[] videoFlag = new Boolean[16];

    public int MID = 1000;

    public Boolean[] getvideoFlag() {
        return videoFlag;
    }

    public void setvideoFlag(Boolean someVariable, int index) {
        this.videoFlag[index] = someVariable;
    }
}