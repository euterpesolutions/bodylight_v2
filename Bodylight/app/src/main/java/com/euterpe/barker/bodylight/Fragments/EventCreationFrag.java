package com.euterpe.barker.bodylight.Fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.euterpe.barker.bodylight.Objects.CalendarObject;
import com.euterpe.barker.bodylight.Preferences.CalPrefHelper;
import com.euterpe.barker.bodylight.R;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Christopher Waldron on 3/27/18.
 * This holds tools to define an event object.
 */

public class EventCreationFrag extends CalendarFragment {

    static final String kTAG = "EventCreationFrag";
    private CalPrefHelper calPrefHelper;
    private static final int kFIFTEEN_MINUTES = 900000;

    /* Variable Declarations */
    private View view;
    private Button date_picker_button;
    private Button time_picker_button;
    private Button add_to_local_calendar;
    private Button add_to_google_calendar;
    private Calendar local_calendar_instance = Calendar.getInstance(Locale.getDefault());
    private CalendarObject newEventObject = new CalendarObject();
    private String event_date_string;
    private String event_time_standard;
    private String event_time_string;
    private String event_exercise_string;
    private Spinner exercise_select_spinner;

    DatePickerDialog.OnDateSetListener dateSetListener;

    // Default Empty Constructor.
    public EventCreationFrag() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Establish connection to preferences.
        calPrefHelper = new CalPrefHelper(getContext());

        // Declare main view.
        view = inflater.inflate(R.layout.event_creation_frag, container, false);
        exercise_select_spinner = view.findViewById(R.id.video_spinner);

        // Declare Buttons.
        date_picker_button = view.findViewById(R.id.event_open_date_picker);
        time_picker_button = view.findViewById(R.id.event_open_time_picker);
        add_to_google_calendar = view.findViewById(R.id.export_to_google);

        date_picker_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDatePickerDialog();
            }
        });
        time_picker_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowTimePickerDialog();
            }
        });
        ConfigureExerciseSpinner();


        add_to_google_calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExportToGoogle();
            }
        });

        return view;
    }


    private void ShowDatePickerDialog () {
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                event_date_string = (month + 1) + "/" +  dayOfMonth + "/" + year;
                Log.d(kTAG, event_date_string);
                newEventObject.setScheduled_date(event_date_string);
                date_picker_button.setText(event_date_string);
            }
        };
        new DatePickerDialog(getContext(),dateSetListener,
                local_calendar_instance.get(Calendar.YEAR) ,
                local_calendar_instance.get(Calendar.MONTH),
                local_calendar_instance.get(Calendar.DAY_OF_MONTH)).show();

    }

    public void ShowTimePickerDialog () {
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minuteOfHour) {
                event_time_string = hourOfDay + ":" + minuteOfHour;
                event_time_standard = MilitaryToTwelve(hourOfDay, minuteOfHour);
                newEventObject.setScheduled_time(event_time_string);
                Log.d(kTAG, "Selected Time: " + newEventObject.getScheduled_time());
                time_picker_button.setText(event_time_string);
            }
        };
        new TimePickerDialog(getContext(), onTimeSetListener , Calendar.HOUR_OF_DAY,
                Calendar.MINUTE, false).show();
    }

    public void ConfigureExerciseSpinner () {
        exercise_select_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                event_exercise_string = parent.getItemAtPosition(position).toString();
                Log.d(kTAG, event_exercise_string);
                newEventObject.setExercise_name(event_exercise_string);
                exercise_select_spinner.clearFocus();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.video_exercise_list,android.R.layout.simple_spinner_item);
        exercise_select_spinner.setAdapter(arrayAdapter);

    }

    private void ExportToGoogle(){
        // The Google implementation requires getting time in milliseconds and thus it has
        // to be in date format.
        Log.d(kTAG, "Sending Event Data to Google Calendar");
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                        newEventObject.GetTimeInMilliseconds())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                        newEventObject.GetTimeInMilliseconds() + kFIFTEEN_MINUTES)
                .putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
                .putExtra(CalendarContract.Events.TITLE, "Bodylight: "
                        + newEventObject.getExercise_name());

        startActivity(intent);
//                // End Google
    }

    private void ExportToBoth() {
        ExportToGoogle();
    }


    // Helper /Formatting Methods
    private String MilitaryToTwelve (int hour, int minute) {
        String s = "";
        Boolean isPM = false;
        if (hour >= 12) {
            hour = hour - 12;
            isPM = true;
        }
        if (hour == 0 ) {
            hour = 12;
        }
        if (isPM) {
            s += FormatTime(hour, minute) + " PM";
        } else {
            s += FormatTime(hour, minute) + " AM";
        }

        return s;
    }
    private String FormatTime (int hour, int minute) {
        String s;
        s = Integer.toString(hour) + ":" + Integer.toString(minute);
        return s;
    }
}