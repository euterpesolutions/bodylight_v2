/*
 * Author: Christopher Waldron
 * Last Update: March 22, 2018
 *
 * Details:
 *   Currently, this has all date and time selection functionality as well as the ability to export
 *   event selections to the Google Calendar. Right now the data collection and variable assignment
 *   is
 */
package com.euterpe.barker.bodylight.Fragments;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;

import com.euterpe.barker.bodylight.Objects.CalendarObject;
import com.euterpe.barker.bodylight.Preferences.CalPrefHelper;
import com.euterpe.barker.bodylight.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class CalendarFragment extends DialogFragment  {

    private static final String kTAG = "CalendarFragment";
    private static final int kFIFTEEN_MINUTES = 900000;

    Calendar calendar;
    CalendarView calendarView; // Save this var.
    CalPrefHelper calPrefHelper;
    String date_for_search;
    Date current_date;
    DateFormat dateNoTime = new SimpleDateFormat("MM/dd/yyyy");
    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
    View view;

    Button create_event;
    GoogleSignInAccount user_account;
    String selected_date;

    private TextView reminder_list_view;
    private Button view_all_events;
    private Button remove_all_events;

    CalendarObject calendarObject = new CalendarObject();

    private OnFragmentInteractionListener mListener;

    public CalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    /**
     * Created By: Christopher Waldron
     * This holds the main functionality.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Calender");
        //Title Change on clicking to enter the Calender Fragment

        calPrefHelper = new CalPrefHelper(getContext());
        // Check that the user is signed in to access their calendar.
        user_account = GoogleSignIn.getLastSignedInAccount(getContext());
        isAccountValid(user_account);

        // Handle Views
        view = inflater.inflate(R.layout.fragment_calendar, container, false);
        reminder_list_view = view.findViewById(R.id.event_reminder_list);
        calendarView = view.findViewById(R.id.event_calendar);

        // Load all current reminders.
        calPrefHelper.ReadAllEventPreferences(reminder_list_view);

        // Establish bases.
        calendar = Calendar.getInstance(Locale.getDefault());
        selected_date = dateNoTime.format(new Date());

        // Declare buttons
        create_event = view.findViewById(R.id.create_event);
        //view_all_events = view.findViewById(R.id.view_all_events);
        remove_all_events = view.findViewById(R.id.remove_all_events);

        // Declare Listeners.

        create_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(kTAG, "Creating New Event: ");
                Fragment fragment = new EventCreationFrag();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_placeholder, fragment)
                        .addToBackStack(null).commit();

            }
        });

        //view_all_events.setOnClickListener(new View.OnClickListener() {
           // @Override
          //  public void onClick(View v) {
                //calPrefHelper.ReadAllEventPreferences(reminder_list_view);
            //}
        //});

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                date_for_search = (month + 1) + "/" +  dayOfMonth + "/" + year;
                Log.d(kTAG, date_for_search);
                calPrefHelper.findEvent(reminder_list_view, date_for_search);

            }
        });

        remove_all_events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reminder_list_view.setText("");
                calPrefHelper.removeAllPreferences();
            }
        });

        calPrefHelper.ReadAllEventPreferences(reminder_list_view);

        return view;
    }

    /* Begin helping methods */
    private void isAccountValid (GoogleSignInAccount account_param) {
        if (account_param == null) {
            Log.d(kTAG, "No Google Account Signed In!");
            AlertDialog.Builder alert = new AlertDialog.Builder(getContext())
                    .setTitle(R.string.no_account_title)
                    .setMessage(R.string.no_account_message)
                    .setPositiveButton("Yes!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Go to sign in
                            getFragmentManager().popBackStack();
                            Fragment fragment = new ProfileFragment();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_placeholder, fragment)
                                    .addToBackStack(null).commit();

                        }
                    })
                    .setNegativeButton("No Thanks!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Dont sign in.
                            dialog.dismiss();
                        }
                    })
                    .setCancelable(false);
            alert.show();
        }

    }


    //    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Button button) {
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}


