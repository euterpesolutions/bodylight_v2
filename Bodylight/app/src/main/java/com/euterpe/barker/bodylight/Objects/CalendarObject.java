package com.euterpe.barker.bodylight.Objects;

import android.app.Application;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by CJ on 3/22/18.
 * This class will handle event objects including date and string processing.
 */

public class CalendarObject extends Application {
    private static final String kTAG = "CalendarObject";

    private String scheduled_date = "";
    private String scheduled_time = "";
    private String exercise_name = "";
    private String date_time_key = "";
    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");



    public CalendarObject() {
    }

    public CalendarObject(String passed_date, String passed_time, String passed_exercise) {
        scheduled_date = passed_date;
        scheduled_time = passed_time;
        exercise_name = passed_exercise;
    }

    public String getScheduled_date() {
        return scheduled_date;
    }

    public void setScheduled_date(String scheduled_date) {
        this.scheduled_date = scheduled_date;
    }

    public String getScheduled_time() {
        return scheduled_time;
    }

    public void setScheduled_time(String scheduled_time) {
        this.scheduled_time = scheduled_time;
    }

    public String getExercise_name() {
        return exercise_name;
    }

    public void setExercise_name(String exercise_name) {
        this.exercise_name = exercise_name;
    }

    public String getDate_time_key() {
        date_time_key = "";
        date_time_key = this.scheduled_date + " " + this.scheduled_time;
        return date_time_key;
    }


    /**
    * Created By: Christopher Waldron
    */

    private String MilitaryToTwelve (int hour, int minute) {
        String s = "";
        Boolean isPM = false;
        if (hour >= 12) {
            hour = hour - 12;
            isPM = true;
        }
        if (hour == 0 ) {
            hour = 12;
        }
        if (isPM) {
            s += FormatTime(hour, minute) + " PM";
        } else {
            s += FormatTime(hour, minute) + " AM";
        }

        return s;
    }
    private String FormatTime (int hour, int minute) {
        String s;
        s = Integer.toString(hour) + ":" + Integer.toString(minute);
        return s;
    }

    private Date StringToDate (String date_string) {
        Log.d(kTAG, "Converting: " + date_string + " to Date Format.");
        Date d = new Date();
        try {
            d = dateFormat.parse(date_string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public String ToString () {
        String s = "";
        s += scheduled_date + " " + scheduled_time + " - " + exercise_name;
        Log.d(kTAG, s);
        return s;
    }

    public long GetTimeInMilliseconds () {
        long millis = 0;
        String temp_date_string = "";
        temp_date_string = this.scheduled_date + " " + scheduled_time;
        try {
            Date temp_date = dateFormat.parse(temp_date_string);
            millis =  temp_date.getTime();
        } catch (ParseException e) {
            Log.d(kTAG, "Unable to Parse Event Time to Millis.");
        }
        return millis;
    }

}
