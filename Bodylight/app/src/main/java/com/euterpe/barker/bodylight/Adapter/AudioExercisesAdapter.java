
    /**
     * Created by CJ on 11/21/17.
     * Builds the cards for the exercise list and sets their click values
     */

    package com.euterpe.barker.bodylight.Adapter;

    import android.app.Fragment;
    import android.content.SharedPreferences;
    import android.support.v7.widget.CardView;
    import android.support.v7.widget.RecyclerView;
    import android.view.View;
    import android.widget.ImageView;
    import android.widget.TextView;
    import android.view.ViewGroup;
    import android.view.LayoutInflater;
    import android.support.v7.app.AppCompatActivity;

    import com.euterpe.barker.bodylight.Fragments.AudioPlayerFragment;
    import com.euterpe.barker.bodylight.Fragments.VideoFragment;
    import com.euterpe.barker.bodylight.R;
    import com.euterpe.barker.bodylight.Interfaces.ItemClickListener;
    import android.content.Context;

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        //For handling button clicks
        private ItemClickListener itemClickListener;
        public final TextView textView;
        public final TextView lenthText;
        public final TextView numberView;
        public final CardView card;
        public final ImageView img;
        public int thumbnailCount;

        //Constructor for handling clicks
        public ViewHolder(View view) {
            super(view);

            //Text box, and checkbox for if this card has been watched before.
            textView = view.findViewById(R.id.card_text);
            lenthText = view.findViewById(R.id.watched_Text);
            numberView = view.findViewById(R.id.textView4);
            card = view.findViewById(R.id.card);
            img = view.findViewById(R.id.thumbnail);
            thumbnailCount = 0;

            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        //Listener for clicks
        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        //The onClick behavior
        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view,getAdapterPosition(), false);
        }
        //Long clicks behave the same as short for now
        @Override
        public boolean onLongClick(View view) {
            itemClickListener.onClick(view,getAdapterPosition(), true);
            return false;
        }

    }
    public class AudioExercisesAdapter extends RecyclerView.Adapter<ViewHolder> {

        //This contains the names of the exercises
        private String[] exercise_list;
        private Context mContext;
        //Initializes the adapter
        public AudioExercisesAdapter(String[] data_list, Context context)
        {
            mContext = context;
            exercise_list = data_list;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 3 || position == 5 || position == 7 || position == 9 || position == 13) {
                return 1;
            } else if (position == 16){
                return 2;
            } else {
                return 0;
            }
        }

        //Creates the cards
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new card
            int blue = this.mContext.getResources().getColor(R.color.highlightBlue);
            if (viewType == 1)
            {
                View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.video_card, parent, false);
                return new ViewHolder(view);
            }
            if(viewType == 2)
            {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.deep_practice_card, parent, false);
                return new ViewHolder(view);
            }
            else
            {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.exercise_card, parent, false);
                return new ViewHolder(view);
            }

            //return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, int position) {
            String[] name_array = mContext.getResources().getStringArray(R.array.combined_exercise_list);
            String[] length_array = mContext.getResources().getStringArray(R.array.recording_lengths_list);
            String[] number_array = mContext.getResources().getStringArray(R.array.combined_number_list);
            int[] thumb_array = {0,0,0,R.drawable.video1,0, R.drawable.video2,0, R.drawable.video3,0, R.drawable.video4,0,0,0, R.drawable.video5,0,0,0};
            final int itemType = getItemViewType(position);
            viewHolder.textView.setText(name_array[position]);

            if (itemType == 0) {
                viewHolder.numberView.setText(number_array[position]);
            }

            if (itemType == 1) {
                viewHolder.img.setImageResource(thumb_array[position]);
            }

            SharedPreferences pref = mContext.getSharedPreferences("AudioFlags", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            String flagPos = "" + position;
            Boolean flag = pref.getBoolean(flagPos, false);

            viewHolder.textView.setText(name_array[position]);
            viewHolder.lenthText.setText(length_array[position]);


            //Sets the onclick listener to pull up the media player fragment when clicked
            viewHolder.setItemClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    if (itemType == 1) {
                        AppCompatActivity appCompatActivity = (AppCompatActivity) view.getContext();

                        if (!isLongClick) {
                            AudioPlayerFragment audioPlayerFragment = new AudioPlayerFragment();
                            Fragment fragment =
                                    VideoFragment.newInstance(viewHolder.textView.getText());
                            appCompatActivity.getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_placeholder, fragment, "findThisFragment")
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }
                    else {
                        AppCompatActivity appCompatActivity = (AppCompatActivity) view.getContext();
                        if (!isLongClick) {
                            AudioPlayerFragment audioPlayerFragment = new AudioPlayerFragment();
                            Fragment fragment =
                                    audioPlayerFragment.newInstance(viewHolder.textView.getText());
                            appCompatActivity.getFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_placeholder, fragment, "findThisFragment")
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return exercise_list.length;
        }
    }
