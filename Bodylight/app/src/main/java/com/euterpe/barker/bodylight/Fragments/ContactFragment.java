package com.euterpe.barker.bodylight.Fragments;

/**
 * Created by CJ on 12/6/17.
 */
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.euterpe.barker.bodylight.R;

public class ContactFragment extends Fragment {

    private View view;
    private WebView browser;


    public ContactFragment() {

    }

    //Constructor
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Contact");
        // Upon entering the Information Fragment, Toolbar title is changed to Information
        view = inflater.inflate(R.layout.fragment_contact, container, false);
        return view;
    }

}
