    package com.euterpe.barker.bodylight.Fragments;

    import android.content.pm.ActivityInfo;
    import android.os.Bundle;
    import android.app.Fragment;
    import android.support.v7.app.AppCompatActivity;
    import android.support.v7.widget.RecyclerView;
    import android.support.v7.widget.Toolbar;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.support.v7.widget.LinearLayoutManager;

    import com.euterpe.barker.bodylight.Adapter.AudioExercisesAdapter;
    import com.euterpe.barker.bodylight.Interfaces.DrawerLocker;
    import com.euterpe.barker.bodylight.R;

    public class ExerciseViewFragment extends Fragment {

        private RecyclerView recyclerView;
        private RecyclerView.Adapter adapter;
        private RecyclerView.LayoutManager layoutManager;

        private String[] exercise_list;
        private int exercise_count = 17;

        public ExerciseViewFragment() {
            // Required empty public constructor
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            initializeData();
        }

        //Creation constructor
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            toolbar.setTitle("Allez-Up! Practice Sessions");
            //Upon clicking the Main exercises button the toolbar title is changed

            // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.fragment_exercise_view, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);


            // Pass data to adapter/recycler view.
            adapter = new AudioExercisesAdapter(exercise_list, this.getContext());
            recyclerView.setAdapter(adapter);
            ((DrawerLocker) getActivity()).setDrawerEnabled(true);
            ((AppCompatActivity)getActivity()).getSupportActionBar().show();
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            return view;
        }

        //Used to create the list of exercise cards
        private void initializeData() {
            exercise_list = new String[exercise_count];
            for (int i = 0; i < exercise_count; i++) {
                exercise_list[i] = "Exercise " + (i+1);
            }
        }
    }
