package com.euterpe.barker.bodylight.Objects;

import android.util.Log;

import java.util.Date;

/**
 * Created by CJ on 2/22/18.
 */

public class CustomEvent {

    private static final String kTAG = "CustomEvent::";

    private int event_hour;
    private int event_minute;
    private Date event_date;


    private String full_time;
    private Date full_date;

    public int getEvent_hour() {
        return event_hour;
    }

    public void setEvent_hour(int event_hour) {
        this.event_hour = event_hour;
    }

    public int getEvent_minute() {
        return event_minute;
    }

    public void setEvent_minute(int event_minute) {
        this.event_minute = event_minute;
    }

    public Date getEvent_date() {
        return event_date;
    }

    public void setEvent_date(Date event_date) {
        this.event_date = event_date;
    }

    public String getFull_time() {
        return full_time;
    }

    public void setFull_time(String full_time) {
        this.full_time = full_time;
    }

    public Date getFull_date() {
        return full_date;
    }

    public void setFull_date(Date full_date) {
        this.full_date = full_date;
    }

    public CustomEvent() {}

    public CustomEvent(Date pick_date, int pick_hour, int pick_minute) {
        Log.d(kTAG, "Creating New Custom Event");
        setEvent_date(pick_date);
        setEvent_hour(pick_hour);
        setEvent_minute(pick_minute);
    }



}
