package com.euterpe.barker.bodylight;

import com.euterpe.barker.bodylight.Preferences.TimePickerPreference;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import android.content.Context;
import android.util.AttributeSet;
/**
 * Created by juwan on 2/4/18.
 */

public class TimePickerPreferenceTest {
    Context mMockContext;
    AttributeSet mMockattrs;
    public void getTimeTest()
    {
        TimePickerPreference ObjectUT = new TimePickerPreference(mMockContext, mMockattrs);
        ObjectUT.setTime("three");

        String testval = ObjectUT.getTime();
        assertThat(testval, is ("three"));

    }
    public void setTimeTest()
    {
        TimePickerPreference ObjectUT = new TimePickerPreference(mMockContext, mMockattrs);
        ObjectUT.setTime("three");

        String testval = ObjectUT.getTime();
        assertThat(testval, is ("three"));
    }
    public void getHourTest()
    {
        TimePickerPreference ObjectUT = new TimePickerPreference(mMockContext, mMockattrs);
        ObjectUT.setHour(3);

        int testval = ObjectUT.getHour();
        assertThat(testval, is (3));
    }
    public void setHourTest()
    {
        TimePickerPreference ObjectUT = new TimePickerPreference(mMockContext, mMockattrs);
        ObjectUT.setTime("three");

        String testval = ObjectUT.getTime();
        assertThat(testval, is ("three"));
    }
}
