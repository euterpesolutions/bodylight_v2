package com.euterpe.barker.bodylight;

import com.euterpe.barker.bodylight.Preferences.PrefManager;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import android.content.Context;
/**
 * Created by juwan on 2/4/18.
 */

public class PrefManagerTest {
    Context mMockContext;
    public void isFirstTimeLaunchTest()
    {
        PrefManager ObjectUT = new PrefManager(mMockContext);
        ObjectUT.setFirstTimeLaunch(false);

        boolean testval = ObjectUT.isFirstTimeLaunch();
        assertThat(testval, is (false));
    }

    public void setFirstTimeLaunchTest()
    {
        PrefManager ObjectUT = new PrefManager(mMockContext);
        ObjectUT.setFirstTimeLaunch(false);

        boolean testval = ObjectUT.isFirstTimeLaunch();
        assertThat(testval, is (false));
    }
}
