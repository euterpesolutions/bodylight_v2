

/**
 * Created by juwan on 2/4/18.
 */

package com.euterpe.barker.bodylight;
import android.content.Context;
import android.content.SharedPreferences;

import com.euterpe.barker.bodylight.Preferences.SettingsPrefs;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Created by CJ on 12/4/17.
 */

//Setting getters and setters



public class SettingsPrefsTest {

    private final String KEY_PREF_FILE = "myPrefs";
    private final String KEY_FREQUENCY = "notification_frequency";
    private final String KEY_TIME = "notification_time";
    private final String KEY_NOTIF_BOOL = "isEnabled";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    Context mMockContext;


    public void getFrequencyTest()
    {

        SettingsPrefs ObjectUT = new SettingsPrefs(mMockContext);
        String testval = "Weekly";
        ObjectUT.setFrequency(testval);
        assertThat(ObjectUT.getFrequency(), is ("Daily"));
    }

    public void setFrequencyTest()
    {
        SettingsPrefs ObjectUT = new SettingsPrefs(mMockContext);
        String testval = "Weekly";
        ObjectUT.setFrequency(testval);
        assertThat(ObjectUT.getFrequency(), is ("Weekly"));
    }


    public void getTimeTest()
    {
        SettingsPrefs ObjectUT = new SettingsPrefs(mMockContext);
        String testval = "time";
        ObjectUT.setTime(testval);
        assertThat(ObjectUT.getTime(), is ("time"));
    }
    public void setTimeTest()
    {
        SettingsPrefs ObjectUT = new SettingsPrefs(mMockContext);
        String testval = "time";
        ObjectUT.setTime(testval);
        assertThat(ObjectUT.getTime(), is ("time"));

    }


}