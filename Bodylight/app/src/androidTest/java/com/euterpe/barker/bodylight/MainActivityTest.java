package com.euterpe.barker.bodylight;

import static android.support.test.espresso.action.ViewActions.pressBack;

import org.junit.Rule;
import org.junit.Test;

import android.support.test.rule.ActivityTestRule;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
/**
 * Created by Kenneth Richardson on 4/29/2018.
 */

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mMainActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Test
    public void onCreate() throws Exception {
        //Opens up the main activity toolbar

        Thread.sleep(3000);
        onView(withId(R.id.toolbar))
                .perform(pressBack());
        Thread.sleep(3000);
        onView(withId(R.id.toolbar))
                .perform(pressBack());
        Thread.sleep(3000);
        onView(withId(R.id.toolbar))
                .perform(pressBack());
        Thread.sleep(3000);
        onView(withId(R.id.toolbar))
                .perform(pressBack());
        Thread.sleep(3000);
        onView(withId(R.id.toolbar))
                .perform(pressBack());
        Thread.sleep(3000);
        onView(withId(R.id.toolbar))
                .perform(pressBack());

    }

}