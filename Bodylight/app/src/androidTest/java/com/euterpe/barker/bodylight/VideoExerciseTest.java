package com.euterpe.barker.bodylight;

import static android.support.test.espresso.action.ViewActions.pressBack;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import android.support.test.rule.ActivityTestRule;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;

/**
 * Created by Kenneth Richardson on 4/29/2018.
 */

public class VideoExerciseTest {

    @Rule
    public ActivityTestRule<MainActivity> VideoExerciseRule = new ActivityTestRule<>(
            MainActivity.class);

    @Before
    public void init() {

        VideoExerciseRule.getActivity()
                .getSupportFragmentManager().beginTransaction();
    }

    @Test
    public void onCreate() throws Exception {
        //Opens up the main activity toolbar
        onView(allOf(withId(R.id.card_text), withText("Introduction")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Moving The Head")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Tips for the Head")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Arms")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Tips for Arms")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Chest")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Tips for Chest")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Tips for Chest")))
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp());
                Thread.sleep(1000);
        onView(allOf(withId(R.id.card_text), withText("Hips")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Tips for Hips")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Tips for Hips")))
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp())
                .perform(swipeUp());
                Thread.sleep(1000);
        onView(allOf(withId(R.id.card_text), withText("Legs")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Tips for Legs")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Feet")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Tips for Feet")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Rolling Down the Spine")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("Tips for the Spine")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(2500);
        onView(allOf(withId(R.id.card_text), withText("BONUS: Jaw Exercises")))
                .perform(click());
                Thread.sleep(10000);
        onView(withId(R.id.videoView))
                .perform(pressBack());
                Thread.sleep(1000);
        onView(withId(R.id.toolbar))
                .perform(pressBack());
                Thread.sleep(1000);
    }
}