package com.euterpe.barker.bodylight;

import static android.support.test.espresso.action.ViewActions.pressBack;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import android.support.test.rule.ActivityTestRule;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;

/**
 * Created by Kenneth Richardson on 4/29/2018.
 */

public class AudioExerciseTest {

    @Rule
    public ActivityTestRule<MainActivity> AudioExerciseRule = new ActivityTestRule<>(
            MainActivity.class);

    @Before
    public void init() {

        AudioExerciseRule.getActivity()
                .getSupportFragmentManager().beginTransaction();
    }

    @Test
    public void onCreate() throws Exception {
        //Opens up the main activity toolbar
        onView(allOf(withId(R.id.card_text), withText("Exercise 1")))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.audio_play_button))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.audio_forward_button))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.audio_forward_button))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.audio_backward_button))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.audio_backward_button))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.toolbar))
                .perform(pressBack());
                Thread.sleep(2000);
        onView(allOf(withId(R.id.card_text), withText("Exercise 2")))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.audio_play_button))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.audio_forward_button))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.audio_forward_button))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.audio_backward_button))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.audio_backward_button))
                .perform(click());
                Thread.sleep(2000);
        onView(withId(R.id.toolbar))
                .perform(pressBack())
                .perform(pressBack());

    }
}