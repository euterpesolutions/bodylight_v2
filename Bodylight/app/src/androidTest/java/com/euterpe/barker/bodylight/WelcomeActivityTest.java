package com.euterpe.barker.bodylight;

import android.support.test.runner.AndroidJUnit4;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import android.support.test.rule.ActivityTestRule;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)

/**
 * Created by Kenny and Justin on 2/3/2018.
 */
public class WelcomeActivityTest {

    @Rule
    public ActivityTestRule<WelcomeActivity> mWelcomeActivityRule = new ActivityTestRule<>(
            WelcomeActivity.class);
    @Test
    public void onCreate() throws Exception {
        //clicks the next button 3 times to reach the final welcome screen
        onView(withId(R.id.btn_next))
                .perform(click())
                .perform(click())
                .perform(click());
        //swipes through all welcome screens backwards then forwards
        onView(withId(R.id.view_pager))
                .perform(swipeRight())
                .perform(swipeRight())
                .perform(swipeRight())
                .perform(swipeLeft())
                .perform(swipeLeft())
                .perform(swipeLeft())
                .perform(swipeRight());
        //clicks the skip button to close the welcome screen
        onView(withId(R.id.btn_skip))
                .perform(click());
    }

}